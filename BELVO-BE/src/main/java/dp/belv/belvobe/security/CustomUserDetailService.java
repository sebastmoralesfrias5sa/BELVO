package dp.belv.belvobe.security;


import dp.belv.belvobe.Domain.Entities.UserBelvo;
import dp.belv.belvobe.Domain.Interfaces.Repositories.IUserRepository;
import dp.belv.belvobe.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    IUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String usernameorEmail) {
        System.out.println("paso "+ usernameorEmail);
        UserBelvo usuario = userRepository.findByEmail(usernameorEmail).orElseThrow(()->new ResourceNotFoundException("usuario","username",usernameorEmail));
        System.out.println("enconrtre a "+usuario);
        Set<GrantedAuthority> authorithies = usuario.getRoles().stream().map((elem) -> new SimpleGrantedAuthority(elem.getName())).collect(Collectors.toSet());
        System.out.println("olaaa  "+usuario);
        System.out.println(authorithies);

        return new User(usuario.getEmail(),usuario.getPassword(),authorithies);
    }
}
