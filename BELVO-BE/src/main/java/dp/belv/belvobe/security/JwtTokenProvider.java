package dp.belv.belvobe.security;

import dp.belv.belvobe.exception.BlogAPIException;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;

@Component
public class JwtTokenProvider {

    @Value("${app-jwt-secret}")
    private String jwtSecret;
    @Value("${app-jwt-expirationdatemiliseconds}")
    private  Long tokenexpirationtime;


    public String generateToken (Authentication authentication){
        String nombre = authentication.getName();
        Date expiretime = new Date(new Date().getTime() +tokenexpirationtime);



        String token = Jwts.builder().setSubject(nombre).setIssuedAt(new Date()).setExpiration(expiretime).signWith(key()).compact();
        System.out.println("te envio el token de generate token  "+token );
        return token ;
    }

    private Key key(){
        return Keys.hmacShaKeyFor(Decoders.BASE64.decode(jwtSecret));
    }

    public  String getUsername(String token){

        return Jwts.parserBuilder().setSigningKey(key()).build()
                .parseClaimsJws(token).getBody().getSubject();

    }

    public boolean validatetoken(String token){
        try {

            Jwts.parserBuilder().setSigningKey(key()).build()
                    .parse(token);
        }
        catch (Exception e){
            return false;
        }
        return  true;
    }

}
