package dp.belv.belvobe.Domain.DTO;

import lombok.Data;

@Data
public class LoginDTO {
    String email;

    String password;
}
