package dp.belv.belvobe.Domain.DTO;

import lombok.Data;

@Data
public class TokenDTO {

    String name;
}
