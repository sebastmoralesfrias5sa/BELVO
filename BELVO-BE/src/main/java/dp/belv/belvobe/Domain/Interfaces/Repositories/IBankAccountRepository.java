package dp.belv.belvobe.Domain.Interfaces.Repositories;

import dp.belv.belvobe.Domain.Entities.BankAccount;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IBankAccountRepository extends CrudRepository<BankAccount, Integer> {

}
