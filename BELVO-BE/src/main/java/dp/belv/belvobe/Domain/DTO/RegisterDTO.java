package dp.belv.belvobe.Domain.DTO;

import lombok.Data;

@Data
public class RegisterDTO {

    String userName;
    int cellPhoneNumber;
    String email;

    String password;

}
