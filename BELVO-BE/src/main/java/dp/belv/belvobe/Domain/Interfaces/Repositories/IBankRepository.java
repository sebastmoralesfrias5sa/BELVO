package dp.belv.belvobe.Domain.Interfaces.Repositories;

import dp.belv.belvobe.Domain.Entities.Bank;
import dp.belv.belvobe.Domain.Entities.UserBelvo;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IBankRepository extends CrudRepository<Bank, Integer> {


}
