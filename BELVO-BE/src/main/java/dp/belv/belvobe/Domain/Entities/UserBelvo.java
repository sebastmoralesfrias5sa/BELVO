package dp.belv.belvobe.Domain.Entities;


import jakarta.persistence.*;
import lombok.Data;

import java.util.Set;

@Entity
@Data
@Table

public class UserBelvo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    String userName;
    int cellPhoneNumber;
    String email;

    String password;

    @OneToMany(mappedBy = "userBelvo",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    Set<BankAccount> BankAccounts;

    @ManyToMany (fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinTable(name = "usuarios-roles",joinColumns = @JoinColumn(name = "users_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "roles_id",referencedColumnName = "id"))
    Set<Role> roles;

}
