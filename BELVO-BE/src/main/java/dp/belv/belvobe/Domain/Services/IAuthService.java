package dp.belv.belvobe.Domain.Services;

import dp.belv.belvobe.Domain.DTO.LoginDTO;
import dp.belv.belvobe.Domain.DTO.RegisterDTO;
import dp.belv.belvobe.Domain.Entities.UserBelvo;

public interface IAuthService {

    String login(LoginDTO loginDTO);
    String register(RegisterDTO registerDTO);

    Boolean validateToken(String token);
}
