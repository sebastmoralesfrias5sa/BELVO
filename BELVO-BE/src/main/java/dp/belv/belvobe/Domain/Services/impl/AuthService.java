package dp.belv.belvobe.Domain.Services.impl;

import dp.belv.belvobe.Domain.DTO.LoginDTO;
import dp.belv.belvobe.Domain.DTO.RegisterDTO;
import dp.belv.belvobe.Domain.Entities.Role;
import dp.belv.belvobe.Domain.Entities.UserBelvo;
import dp.belv.belvobe.Domain.Interfaces.Repositories.IRoleRepository;
import dp.belv.belvobe.Domain.Interfaces.Repositories.IUserRepository;
import dp.belv.belvobe.Domain.Services.IAuthService;
import dp.belv.belvobe.exception.BlogAPIException;
import dp.belv.belvobe.security.JwtTokenProvider;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class AuthService implements IAuthService {

    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    IUserRepository userRepository;

    @Autowired
    IRoleRepository roleRepository;

    @Autowired
    ModelMapper modelMapper;


    @Autowired
    PasswordEncoder passwordEncoder;
    @Override
    public String login(LoginDTO loginDTO) {
        System.out.println("estoy en el service");
        Authentication authentication =  authenticationManager.authenticate( new UsernamePasswordAuthenticationToken(loginDTO.getEmail(),loginDTO.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        System.out.println("casi");
        String token = jwtTokenProvider.generateToken(authentication);
        System.out.println("el token es:");
        return token ;
    }

    @Override
    public String register(RegisterDTO registerDTO) {
        if(userRepository.existsByUserName(registerDTO.getUserName())){
            return "Username already exists";
        }
        if(userRepository.existsByEmail(registerDTO.getEmail())){
            return "Email already exists";
        }

        Set<Role> roles = new HashSet<>();
        Role role = roleRepository.findByName("ROLE_USER");
        roles.add(role);
        UserBelvo userBelvo =  modelMapper.map(registerDTO, UserBelvo.class);
        userBelvo.setPassword(passwordEncoder.encode(registerDTO.getPassword()));
        userBelvo.setRoles(roles);
        userRepository.save(userBelvo);
         return "user registered succesfuly";
    }

    @Override
    public Boolean validateToken(String token) {
        return  jwtTokenProvider.validatetoken(token);
    }
}
