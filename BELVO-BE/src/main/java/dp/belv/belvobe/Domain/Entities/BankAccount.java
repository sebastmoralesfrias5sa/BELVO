package dp.belv.belvobe.Domain.Entities;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table
public class BankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;


    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn
    private UserBelvo userBelvo;

    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn
    private Bank bank;


}
