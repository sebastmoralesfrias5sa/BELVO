package dp.belv.belvobe.Domain.Entities;

import jakarta.persistence.*;
import lombok.Data;

import java.util.Set;

@Entity
@Data
@Table
public class Bank {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    String name;
    String imageURL;
    String apiURL;
    @OneToMany(mappedBy = "bank")
    Set<BankAccount> BankAccounts;

}
