package dp.belv.belvobe.Domain.Interfaces.Repositories;

import dp.belv.belvobe.Domain.Entities.Bank;
import dp.belv.belvobe.Domain.Entities.Role;
import org.springframework.data.repository.CrudRepository;

public interface IRoleRepository extends CrudRepository<Role, Integer> {

   Role findByName(String name);
}
