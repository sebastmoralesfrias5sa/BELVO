package dp.belv.belvobe.Domain.Interfaces.Repositories;

import dp.belv.belvobe.Domain.Entities.UserBelvo;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface IUserRepository extends CrudRepository<UserBelvo, Integer> {
    Optional<UserBelvo> findByEmail (String email);

    boolean existsByUserName(String userName);
    boolean existsByEmail(String email);
}
