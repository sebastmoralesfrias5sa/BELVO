package dp.belv.belvobe.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResourceNotFoundException extends  RuntimeException {

    String object;
    String fieldname;
    String fieldvalue;

}
