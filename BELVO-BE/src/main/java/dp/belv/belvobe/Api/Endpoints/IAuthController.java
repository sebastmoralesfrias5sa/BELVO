package dp.belv.belvobe.Api.Endpoints;

import dp.belv.belvobe.Domain.DTO.LoginDTO;
import dp.belv.belvobe.Domain.DTO.RegisterDTO;
import dp.belv.belvobe.Domain.DTO.TokenDTO;
import dp.belv.belvobe.security.JwtAuthResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

public interface IAuthController {

    ResponseEntity<String> register(@RequestBody RegisterDTO registerDTO);

    ResponseEntity<JwtAuthResponse> login(@RequestBody  LoginDTO loginDTO);


    boolean validateToken(@RequestBody TokenDTO tokenDTO);
}
