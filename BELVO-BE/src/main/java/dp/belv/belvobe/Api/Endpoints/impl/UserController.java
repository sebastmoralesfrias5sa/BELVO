package dp.belv.belvobe.Api.Endpoints.impl;

import dp.belv.belvobe.Api.Endpoints.IUserController;
import dp.belv.belvobe.Domain.Entities.UserBelvo;
import dp.belv.belvobe.Domain.Interfaces.Repositories.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/users")
public class UserController implements IUserController {
    @Autowired
    private IUserRepository userRepository;

    @PostMapping
    public ResponseEntity<UserBelvo> createUser(@RequestBody UserBelvo userBelvo) {
        UserBelvo newUserBelvo = userRepository.save(userBelvo);
        return new ResponseEntity<>(newUserBelvo, HttpStatus.CREATED);
    }

    @GetMapping
    public List<UserBelvo> listUser(){
        return  (List<UserBelvo>) userRepository.findAll();
    }


}
