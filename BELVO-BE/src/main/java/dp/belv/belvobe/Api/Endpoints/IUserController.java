package dp.belv.belvobe.Api.Endpoints;

import dp.belv.belvobe.Domain.Entities.UserBelvo;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

public interface IUserController {

     ResponseEntity<UserBelvo> createUser(@RequestBody UserBelvo userBelvo);

}
