package dp.belv.belvobe.Api.Endpoints.impl;

import dp.belv.belvobe.Api.Endpoints.IAuthController;
import dp.belv.belvobe.Domain.DTO.LoginDTO;
import dp.belv.belvobe.Domain.DTO.RegisterDTO;
import dp.belv.belvobe.Domain.DTO.TokenDTO;
import dp.belv.belvobe.Domain.Services.IAuthService;
import dp.belv.belvobe.security.JwtAuthResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/auth")
public class AuthController implements IAuthController {

    @Autowired
    IAuthService authService;

    @PostMapping("/login")
    public ResponseEntity<JwtAuthResponse> login(@RequestBody LoginDTO loginDTO) {
        System.out.println("estoy recibiendo " +loginDTO);
        String response = authService.login(loginDTO);
        JwtAuthResponse jwtAuthResponse = new JwtAuthResponse();
        jwtAuthResponse.setToken(response);
        return ResponseEntity.ok(jwtAuthResponse);
    }

    @PostMapping("/validateToken")
    public boolean validateToken(@RequestBody TokenDTO tokenDTO) {

        return authService.validateToken(tokenDTO.getName());
    }


    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody  RegisterDTO registerDTO) {
        String response = authService.register(registerDTO);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }




}
